//const PORT = 80;
const PORT = process.env.port || 80;

var express = require("express");
var handlebars = require("express-handlebars")
	.create({ defaultLayout: "main" });
var bodyParser = require("body-parser");

var app = express();
app.engine("handlebars", handlebars.engine);
app.set("view engine", "handlebars");

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded(
	{ extended: "true" }
));
app.use(bodyParser.json());
app.use(bodyParser.json(
	{ type: "application/vnd.ap+json" }
));



app.get("/help", function(req, res, nxt) { res.send("alive"); })


require("./test/routes")(app, express);
require("./index/routes")(app, express);




app.listen(PORT)
console.log("Alive on " + PORT);
