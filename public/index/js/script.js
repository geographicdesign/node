document.addEventListener("DOMContentLoaded", onload);

const PAGE_NAME = "Geographic Design";
var canvas = {
	width: 0,
	height: 0,
};

var margin = {
	top: 75,
	bottom: 75 + 15,
	left: 75,
	right: 75 + 15,		
};


function animatePageIntro(container) {
	var node = d3.select(container);

	var titleChar = node.selectAll(".title-char")
		.data(PAGE_NAME.split(""));

	titleChar.enter()
		.append("text")
		.attr("class", "title-char")
		.text(function(d) { return d; });
}


function setCanvas() {
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;	
	console.log("canvas size: ", canvas);
}


function resize() {
	var transition = 
	setCanvas();
	d3.select("#canvas-svg")
		.attr("width", canvas.width)
		.attr("height", canvas.height);

	d3.select("#test-rect")
		.transition()
		.duration(900)
		.transition()
		.ease(d3.easeElastic)
		.attr("width", canvas.width - margin.left - margin.right)
		.attr("height", canvas.height - margin.top - margin.bottom)

		
}






function onload() {
	window.addEventListener("resize", resize)	
	
	var svg = d3.select("body")
		.append("svg")
		.attr("id", "canvas-svg")
		.attr("width", canvas.width)
		.attr("height", canvas.height);

	var rect = svg.append("rect")
		.attr("id", "test-rect")
		.attr("x", margin.left)
		.attr("y", margin.top)
		.attr("width", canvas.width - margin.left - margin.right)
		.attr("height", canvas.height - margin.top - margin.bottom)
		.attr("fill", "blue")
	
	setCanvas();	
	resize();
	
	//animatePageIntro("body");
}


