exports.get = {
	test: function(req, res, nxt) {
		var context = {
			script: [ "../app/js/d3.v4.min.js", "../test/js/script.js" ],
			title: "Test Page",
		}
	
		var params = req.body;

		res.render("test", context);

	},

}
