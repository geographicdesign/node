const DIR = "/test";
const CTRL = require("./controllers");

module.exports = function(app, express) {

	app.get(DIR, CTRL.get.test);

};
