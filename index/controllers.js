exports.get = {
	index: function(req, res, nxt) {
		var context = {
			script: [ "./app/js/d3.v4.min.js", "./index/js/script.js" ],
			title: "Test Page",
		}
	
		var params = req.body;

		res.render("index", context);

	},

}
