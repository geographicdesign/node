const DIR = "/index";
const CTRL = require("./controllers");

module.exports = function(app, express) {
	app.get("/", CTRL.get.index);

};
